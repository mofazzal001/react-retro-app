import React, { Component } from 'react';

class Postit extends Component {

  render() {

    return(
      <div>
          <p>Please fill the Postit</p>
          <div>
              <input type="text" id="title" placeholder="Title" />
          </div>
          <div></div>
          <div>
              <input type="text" id="content" placeholder="Content..." />
          </div>
          <div></div>
          <span>
            <button id="cancel">Cancel</button>
          </span>
          <span>
            <button id="okay" onClick={this.props.onMagicClick} >Okay</button>
          </span>
    </div>
    );
  }
}

export default Postit;
