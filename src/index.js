import React from 'react'
import Dashboard from './components/dashboard.js';
import { render } from 'react-dom';

render(
    <Dashboard/>,
    document.getElementById('app')
);
